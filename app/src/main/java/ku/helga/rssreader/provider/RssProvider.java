package ku.helga.rssreader.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import ku.helga.rssreader.provider.table.ChannelTable;
import ku.helga.rssreader.provider.table.ItemTable;

/**
 * Created by user on 04.06.2015.
 */
public class RssProvider extends ContentProvider {

    public static final String AUTHORITY = "ku.helga.rssprovider";

    private static final int CHANNEL_ALLROWS = 1;
    private static final int CHANNEL_SINGLE_ROW = 2;
    private static final int ITEM_ALLROWS = 3;
    private static final int ITEM_SINGLE_ROW = 4;

    private static final UriMatcher URI_MATCHER;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, ChannelTable.TABLE, CHANNEL_ALLROWS);
        URI_MATCHER.addURI(AUTHORITY, ChannelTable.TABLE + "/#", CHANNEL_SINGLE_ROW);
        URI_MATCHER.addURI(AUTHORITY, ItemTable.TABLE, ITEM_ALLROWS);
        URI_MATCHER.addURI(AUTHORITY, ItemTable.TABLE + "/#", ITEM_SINGLE_ROW);
    }

    private SQLiteOpenHelper mHelper;

    @Override
    public boolean onCreate() {
        mHelper = new RssOpenHelper(getContext(), RssOpenHelper.DATABASE_NAME, null, RssOpenHelper.DATABASE_VERSION);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db;
        try {
            db = mHelper.getWritableDatabase();
        } catch (SQLiteException exc) {
            db = mHelper.getReadableDatabase();
        }
        String groupBy = null;
        String having = null;

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        switch (URI_MATCHER.match(uri)) {
            case CHANNEL_SINGLE_ROW:
                builder.appendWhere(ChannelTable.KEY_ID + "=" + getRowId(uri));
                break;
            case ITEM_SINGLE_ROW:
                builder.appendWhere(ItemTable.KEY_ID + "=" + getRowId(uri));
            default:
                break;
        }
        builder.setTables(getTable(uri));
        return builder.query(db, projection, selection, selectionArgs, groupBy, having, sortOrder);
    }

    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case CHANNEL_ALLROWS:
                return "vnd.android.cursor.dir/vnd.rssprovider." + ChannelTable.TABLE;
            case CHANNEL_SINGLE_ROW:
                return "vnd.android.cursor.item/vnd.rssprovider." + ChannelTable.TABLE;
            case ITEM_ALLROWS:
                return "vnd.android.cursor.dir/vnd.rssprovider." + ItemTable.TABLE;
            case ITEM_SINGLE_ROW:
                return "vnd.android.cursor.item/vnd.rssprovider." + ItemTable.TABLE;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        String nullColumnHack = null;
        long id = db.insert(getTable(uri), nullColumnHack, contentValues);
        if (id > -1) {
            Uri insertedId = ContentUris.withAppendedId(uri, id);
            getContext().getContentResolver().notifyChange(insertedId, null);
            return insertedId;
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        switch (URI_MATCHER.match(uri)) {
            case CHANNEL_SINGLE_ROW:
                selection = ChannelTable.KEY_ID + "=" + getRowId(uri)
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
            case ITEM_SINGLE_ROW:
                selection = ItemTable.KEY_ID + "=" + getRowId(uri)
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
        }
        if (selection == null) {
            selection = "1";
        }
        int deleteCount = db.delete(getTable(uri), selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return deleteCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        switch (URI_MATCHER.match(uri)) {
            case CHANNEL_SINGLE_ROW:
                selection = ChannelTable.KEY_ID + "=" + getRowId(uri)
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
            case ITEM_SINGLE_ROW:
                selection = ItemTable.KEY_ID + "=" + getRowId(uri)
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : "");
                break;
        }
        int updateCount = db.update(getTable(uri), values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return updateCount;
    }

    private static String getTable(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case CHANNEL_ALLROWS:
            case CHANNEL_SINGLE_ROW:
                return ChannelTable.TABLE;
            case ITEM_ALLROWS:
            case ITEM_SINGLE_ROW:
                return ItemTable.TABLE;
            default:
                return null;
        }
    }

    private static String getRowId(Uri uri) {
        return uri.getPathSegments().get(1);
    }
}
