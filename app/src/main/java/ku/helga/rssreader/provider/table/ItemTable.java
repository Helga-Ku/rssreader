package ku.helga.rssreader.provider.table;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import ku.helga.rssreader.provider.RssProvider;

/**
 * Created by user on 04.06.2015.
 */
public class ItemTable {
    public static final String TABLE = ku.helga.rssreader.models.Item.ITEM;
    public static final String KEY_ID = "_id";
    public static final String KEY_CHANNEL = ku.helga.rssreader.models.Channel.CHANNEL;
    public static final String KEY_LINK = ku.helga.rssreader.models.Item.LINK;
    public static final String KEY_TTL = ku.helga.rssreader.models.Item.TITLE;
    public static final String KEY_DESCRIPTION = ku.helga.rssreader.models.Item.DESCRIPTION;

    public static final Uri URI = Uri.parse("content://" + RssProvider.AUTHORITY + "/" + TABLE);

    public static long getChannel(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(KEY_CHANNEL));
    }

    public static String getTitle(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(KEY_TTL));
    }

    public static String getLink(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(KEY_LINK));
    }

    public static String getDescription(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION));
    }

    private static Uri insert(ContentResolver resolver, long channel, String ttl, String desc, String link) {
        ContentValues values = new ContentValues();
        values.put(KEY_CHANNEL, channel);
        values.put(KEY_TTL, ttl);
        values.put(KEY_DESCRIPTION, desc);
        values.put(KEY_LINK, link);
        return resolver.insert(URI, values);
    }

    private static int update(ContentResolver resolver, String link, long channel, String ttl, String desc) {
        ContentValues values = new ContentValues();
        values.put(KEY_CHANNEL, channel);
        values.put(KEY_TTL, ttl);
        values.put(KEY_DESCRIPTION, desc);
        return resolver.update(URI, values, KEY_LINK + "=?", new String[]{link});
    }

    public static boolean insertOrUpdate(ContentResolver resolver, String link, long channel, String ttl, String desc) {
        boolean success;
        final Cursor cursor = resolver.query(URI, null, KEY_LINK + "=?", new String[]{link}, null);
        if (cursor.moveToFirst()) {
            success = update(resolver, link, channel, ttl, desc) > 0;
        } else {
            success = insert(resolver, channel, ttl, desc, link) != null;
        }
        cursor.close();
        return success;
    }

    public static Cursor getItems(ContentResolver resolver, long id) {
        return resolver.query(URI, null, KEY_CHANNEL + "=" + id, null, null);
    }

    public static int deleteItems(ContentResolver resolver, long id) {
        return resolver.delete(URI, KEY_CHANNEL + "=" + id, null);
    }
}
