package ku.helga.rssreader.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import ku.helga.rssreader.provider.table.ChannelTable;
import ku.helga.rssreader.provider.table.ItemTable;

/**
 * Created by user on 04.06.2015.
 */
public class RssOpenHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "channels.sqlite";
    public static final int DATABASE_VERSION = 1;

    private static final String CREATE_CHANNELS_TABLE = "create table " + ChannelTable.TABLE
            + " (" + ChannelTable.KEY_ID + " integer primary key autoincrement, "
            + ChannelTable.KEY_TTL + " text, " + ChannelTable.KEY_LINK + " text, " + ChannelTable.KEY_DESCRIPTION + " text);";

    private static final String CREATE_ITEMS_TABLE = "create table " + ItemTable.TABLE
            + " (" + ItemTable.KEY_ID + " integer primary key autoincrement, "
            + ItemTable.KEY_CHANNEL + " integer, "
            + ItemTable.KEY_TTL + " text, " + ItemTable.KEY_LINK + " text, " + ItemTable.KEY_DESCRIPTION + " text);";

    public RssOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_CHANNELS_TABLE);
        sqLiteDatabase.execSQL(CREATE_ITEMS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
