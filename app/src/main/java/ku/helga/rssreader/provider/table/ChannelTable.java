package ku.helga.rssreader.provider.table;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import ku.helga.rssreader.provider.RssProvider;


/**
 * Created by user on 04.06.2015.
 */
public class ChannelTable {
    public static final String TABLE = ku.helga.rssreader.models.Channel.CHANNEL;
    public static final String KEY_ID = "_id";
    public static final String KEY_LINK = ku.helga.rssreader.models.Channel.LINK;
    public static final String KEY_TTL = ku.helga.rssreader.models.Channel.TITLE;
    public static final String KEY_DESCRIPTION = ku.helga.rssreader.models.Channel.DESCRIPTION;

    public static final Uri URI = Uri.parse("content://" + RssProvider.AUTHORITY + "/" + TABLE);

    public static String getTitle(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(KEY_TTL));
    }

    public static String getLink(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(KEY_LINK));
    }

    public static boolean insertOrUpdate(ContentResolver resolver, String link) {
        boolean success = true;
        final Cursor cursor = resolver.query(URI, null, KEY_LINK + "=?", new String[]{link}, null);
        if (!cursor.moveToFirst()) {
            ContentValues values = new ContentValues();
            values.put(KEY_LINK, link);
            success = resolver.insert(URI, values) != null;
        }
        cursor.close();
        return success;
    }

    public static int updateLink(ContentResolver resolver, long id, String newLink) {
        ItemTable.deleteItems(resolver, id);
        final ContentValues values = new ContentValues();
        values.put(KEY_LINK, newLink);
        values.put(KEY_TTL, "");
        values.put(KEY_DESCRIPTION, "");
        return resolver.update(ContentUris.withAppendedId(URI, id), values, null, null);
    }

    public static int update(ContentResolver resolver, long id, String ttl, String desc) {
        final ContentValues values = new ContentValues();
        values.put(KEY_TTL, ttl);
        values.put(KEY_DESCRIPTION, desc);
        return resolver.update(ContentUris.withAppendedId(URI, id), values, null, null);
    }

    public static int delete(ContentResolver resolver, long id) {
        ItemTable.deleteItems(resolver, id);
        return resolver.delete(ContentUris.withAppendedId(URI, id), null, null);
    }
}
