package ku.helga.rssreader;

import android.content.ContentResolver;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import ku.helga.rssreader.fragments.Channels;
import ku.helga.rssreader.provider.table.ChannelTable;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                getSupportActionBar().setDisplayHomeAsUpEnabled(getSupportFragmentManager().getBackStackEntryCount() > 0);
            }
        });

        addChannels();
        getSupportFragmentManager()
                .beginTransaction().add(R.id.container, Channels.newInstance()).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void addChannels() {
        final ContentResolver resolver = getContentResolver();
        for (String channel : getResources().getStringArray(R.array.channels)) {
            ChannelTable.insertOrUpdate(resolver, channel);
        }
    }
}
