package ku.helga.rssreader.models;

import android.text.TextUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created by user on 04.06.2015.
 */
public class Item extends XmlModel {

    public static final String ITEM = "item";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String LINK = "link";

    private String mTitle;
    private String mLink;
    private String mDescription;

    public Item(XmlPullParser parser) {
        try {
            parse(parser);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getLink() {
        return mLink;
    }

    @Override
    public void parse(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE, Item.ITEM);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (TextUtils.equals(Item.TITLE, name)) {
                mTitle = readText(name, parser);
            } else if (TextUtils.equals(Item.DESCRIPTION, name)) {
                mDescription = readText(name, parser);
            } else if (TextUtils.equals(Item.LINK, name)) {
                mLink = readText(name, parser);
            } else {
                skip(parser);
            }
        }
    }
}
