package ku.helga.rssreader.models;

import android.text.TextUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 04.06.2015.
 */

public class Channel extends XmlModel {

    public static final String CHANNEL = "channel";

    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String LINK = "link";

    private String mTitle;
    private String mLink;
    private String mDescription;

    private List<Item> mItems = new ArrayList<>();

    public Channel(XmlPullParser parser) {
        try {
            parse(parser);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    public String getTitle() {
        return mTitle;
    }

    public List<Item> getItems() {
        return mItems;
    }

    public String getDescription() {
        return mDescription;
    }

    @Override
    public void parse(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE, Channel.CHANNEL);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (TextUtils.equals(Channel.TITLE, name)) {
                mTitle = readText(name, parser);
            } else if (TextUtils.equals(Channel.DESCRIPTION, name)) {
                mDescription = readText(name, parser);
            } else if (TextUtils.equals(Channel.LINK, name)) {
                mLink = readText(name, parser);
            } else if (TextUtils.equals(Item.ITEM, name)) {
                mItems.add(new Item(parser));
            } else {
                skip(parser);
            }
        }
    }
}
