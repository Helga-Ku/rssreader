package ku.helga.rssreader.models;

import android.text.TextUtils;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by user on 04.06.2015.
 */
public class Rss extends XmlModel {

    public static final String RSS = "rss";
    private Channel mChannel;

    public Rss(InputStream is) {
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(is, null);
            parse(parser);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void parse(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.nextTag();
        parser.require(XmlPullParser.START_TAG, NAMESPACE, RSS);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            if (TextUtils.equals(Channel.CHANNEL, parser.getName())) {
                mChannel = new Channel(parser);
            }
        }
    }

    public Channel getChannel() {
        return mChannel;
    }
}
