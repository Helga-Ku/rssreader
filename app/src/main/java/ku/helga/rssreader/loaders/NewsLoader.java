package ku.helga.rssreader.loaders;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;;

import ku.helga.rssreader.models.Channel;
import ku.helga.rssreader.models.Item;
import ku.helga.rssreader.models.Rss;
import ku.helga.rssreader.provider.table.ChannelTable;
import ku.helga.rssreader.provider.table.ItemTable;

/**
 * Created by user on 04.06.2015.
 */
public class NewsLoader extends CursorLoader {

    private final String mChannel;
    private final long mId;

    public NewsLoader(Context context, long id, String channel) {
        super(context);
        mId = id;
        mChannel = channel;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public Cursor loadInBackground() {
        Channel channel = null;
        try {
            final URL url = new URL(mChannel);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            try {
                if (connection.getResponseCode() == 200) {
                    channel = new Rss(connection.getInputStream()).getChannel();
                }
            } finally {
                connection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        updateData(channel);
        return ItemTable.getItems(getContext().getContentResolver(), mId);
    }

    private void updateData(Channel channel) {
        if (channel != null) {
            final ContentResolver resolver = getContext().getContentResolver();
            ChannelTable.update(resolver, mId, channel.getTitle(), channel.getDescription());
            for (Item item : channel.getItems()) {
                ItemTable.insertOrUpdate(resolver, item.getLink(), mId, item.getTitle(), item.getDescription());
            }
        }
    }
}
