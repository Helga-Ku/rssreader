package ku.helga.rssreader.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ku.helga.rssreader.R;

/**
 * Created by user on 04.06.2015.
 */
public class EditChannel extends DialogFragment implements TextWatcher {

    public static final String EXTRA_ID = "extra_id";
    public static final String EXTRA_LINK = "extra_link";

    private Button mEditBtn;
    private EditText mLinkEdt;
    private String mLink;
    private long mId;

    public static DialogFragment newInstance(Fragment targetFmt, int requestCode, long id, String oldLink) {
        final DialogFragment dlg = new EditChannel();
        dlg.setTargetFragment(targetFmt, requestCode);
        final Bundle args = new Bundle();
        args.putLong(EXTRA_ID, id);
        args.putString(EXTRA_LINK, oldLink);
        dlg.setArguments(args);
        return dlg;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mId = getArguments().getLong(EXTRA_ID);
        mLink = getArguments().getString(EXTRA_LINK);
        final View view = View.inflate(getActivity(), R.layout.fmt_add_channel, null);
        mLinkEdt = (EditText) view.findViewById(android.R.id.edit);
        mLinkEdt.setText(mLink);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.ttl_edit_channel)
                .setView(view)
                .setPositiveButton(R.string.btn_edit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final String newLink = mLinkEdt.getText().toString();
                        if (!TextUtils.equals(mLink, newLink)) {
                            final Intent data = new Intent();
                            data.putExtra(EXTRA_ID, mId);
                            data.putExtra(EXTRA_LINK, newLink);
                            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
                        }
                    }
                }).setNegativeButton(R.string.btn_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final Intent data = new Intent();
                        data.putExtra(EXTRA_ID, mId);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
                    }
                });

        final AlertDialog dlg = builder.create();
        dlg.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                mEditBtn = dlg.getButton(AlertDialog.BUTTON_POSITIVE);
            }
        });
        mLinkEdt.addTextChangedListener(this);
        return dlg;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (mEditBtn != null) {
            mEditBtn.setEnabled(TextUtils.getTrimmedLength(editable) > 0);
        }
    }
}
