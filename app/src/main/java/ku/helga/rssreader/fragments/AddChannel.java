package ku.helga.rssreader.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ku.helga.rssreader.R;

/**
 * Created by user on 04.06.2015.
 */
public class AddChannel extends DialogFragment implements TextWatcher {

    public static final String EXTRA_LINK = "extra_link";
    private Button mCreateBtn;
    private EditText mLinkEdt;

    public static DialogFragment newInstance(Fragment targetFmt, int requestCode) {
        final DialogFragment dlg = new AddChannel();
        dlg.setTargetFragment(targetFmt, requestCode);
        return dlg;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View view = View.inflate(getActivity(), R.layout.fmt_add_channel, null);
        mLinkEdt = (EditText) view.findViewById(android.R.id.edit);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.ttl_add_channel)
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final Intent data = new Intent();
                        data.putExtra(EXTRA_LINK, mLinkEdt.getText().toString());
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
                    }
                }).setNegativeButton(android.R.string.cancel, null);

        final AlertDialog dlg = builder.create();
        dlg.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                mCreateBtn = dlg.getButton(AlertDialog.BUTTON_POSITIVE);
                mCreateBtn.setEnabled(false);
            }
        });
        mLinkEdt.addTextChangedListener(this);
        return dlg;
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (mCreateBtn != null) {
            mCreateBtn.setEnabled(TextUtils.getTrimmedLength(editable) > 0);
        }
    }
}
