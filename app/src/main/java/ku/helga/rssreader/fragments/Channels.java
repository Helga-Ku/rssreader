package ku.helga.rssreader.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import ku.helga.rssreader.R;
import ku.helga.rssreader.provider.table.ChannelTable;
import ku.helga.rssreader.provider.table.ItemTable;

/**
 * A placeholder fragment containing a simple view.
 */
public class Channels extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemLongClickListener {

    private CursorAdapter mAdapter;

    public static Channels newInstance() {
        final Channels fmt = new Channels();
        fmt.setArguments(new Bundle());
        return fmt;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fmt_channels, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(R.string.ttl_channels);
        mAdapter = new ChannelsAdapter(getActivity());
        setListAdapter(mAdapter);
        getListView().setOnItemLongClickListener(this);
        getLoaderManager().restartLoader(R.id.loader_channels, null, this);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        final Cursor item = (Cursor) mAdapter.getItem(position);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, News.newInstance(id, ChannelTable.getTitle(item), ChannelTable.getLink(item)))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_channels, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_channel:
                DialogFragment newFragment = AddChannel.newInstance(this, R.id.req_code_add_channel);
                newFragment.show(getFragmentManager(), "dialog");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == R.id.req_code_add_channel) {
                if (data != null) {
                    final String link = data.getStringExtra(AddChannel.EXTRA_LINK);
                    if (ChannelTable.insertOrUpdate(getActivity().getContentResolver(), link)) {
                        getLoaderManager().restartLoader(R.id.loader_channels, null, this);
                    }
                }
            } else if (requestCode == R.id.req_code_edit_channel) {
                final long id = data.getLongExtra(EditChannel.EXTRA_ID, -1);
                final String newLink = data.getStringExtra(EditChannel.EXTRA_LINK);
                if (TextUtils.isEmpty(newLink) && ChannelTable.delete(getActivity().getContentResolver(), id) > 0) {
                    getLoaderManager().restartLoader(R.id.loader_channels, null, this);
                } else if (!TextUtils.isEmpty(newLink) && ChannelTable.updateLink(getActivity().getContentResolver(), id, newLink) > 0) {
                    getLoaderManager().restartLoader(R.id.loader_channels, null, this);
                }
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), ChannelTable.URI, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        final Cursor item = (Cursor) mAdapter.getItem(i);
        DialogFragment newFragment = EditChannel.newInstance(this, R.id.req_code_edit_channel, l, ItemTable.getLink(item));
        newFragment.show(getFragmentManager(), "dialog");
        return true;
    }

    class ChannelsAdapter extends CursorAdapter {

        public ChannelsAdapter(Context context) {
            super(context, null, false);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return View.inflate(context, android.R.layout.simple_list_item_2, null);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ((TextView) view.findViewById(android.R.id.text1)).setText(ChannelTable.getTitle(cursor));
            ((TextView) view.findViewById(android.R.id.text2)).setText(ChannelTable.getLink(cursor));
        }
    }
}
