package ku.helga.rssreader.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ku.helga.rssreader.R;

/**
 * Created by user on 04.06.2015.
 */
public class Post extends Fragment {

    private static final String ARG_TTL = "arg_ttl";
    private static final String ARG_DESCRIPTION = "arg_description";

    private TextView mTtl;
    private TextView mDescription;

    public static Post newInstance(String ttl, String desc){
        final Post fmt = new Post();
        final Bundle args = new Bundle();
        args.putString(ARG_TTL, ttl);
        args.putString(ARG_DESCRIPTION, desc);
        fmt.setArguments(args);
        return fmt;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fmt_post, container, false);
        mTtl = (TextView) view.findViewById(R.id.tv_ttl);
        mDescription = (TextView) view.findViewById(R.id.tv_desc);
        mDescription.setMovementMethod(LinkMovementMethod.getInstance());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mTtl.setText(getArguments().getString(ARG_TTL, ""));
        mDescription.setText(Html.fromHtml(getArguments().getString(ARG_DESCRIPTION, "")));
    }
}
