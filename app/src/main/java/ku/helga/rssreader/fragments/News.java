package ku.helga.rssreader.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import ku.helga.rssreader.R;
import ku.helga.rssreader.loaders.NewsLoader;
import ku.helga.rssreader.provider.table.ItemTable;

/**
 * Created by user on 04.06.2015.
 */
public class News extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>,
        SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_ID = "arg_id";
    private static final String ARG_CHANNEL = "arg_channel";
    private static final String ARG_TTL = "arg_ttl";

    private SwipeRefreshLayout mSwipeLayout;
    private String mChannel;
    private long mId;
    private CursorAdapter mAdapter;

    public static News newInstance(long id, String ttl, String channel) {
        final News fmt = new News();
        final Bundle args = new Bundle();
        args.putLong(ARG_ID, id);
        args.putString(ARG_CHANNEL, channel);
        args.putString(ARG_TTL, ttl);
        fmt.setArguments(args);
        return fmt;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mId = getArguments().getLong(ARG_ID);
        mChannel = getArguments().getString(ARG_CHANNEL);
        final View view = inflater.inflate(R.layout.fmt_news, container, false);
        mSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(getArguments().getString(ARG_TTL));
        mAdapter = new NewsAdapter(getActivity());
        setListAdapter(mAdapter);
        getLoaderManager().initLoader(R.id.loader_news, null, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mSwipeLayout.setOnRefreshListener(this);
    }

    @Override
    public void onPause() {
        mSwipeLayout.setOnRefreshListener(null);
        mSwipeLayout.setRefreshing(false);
        mSwipeLayout.destroyDrawingCache();
        mSwipeLayout.clearAnimation();
        super.onPause();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        final Cursor item = (Cursor) mAdapter.getItem(position);
        getFragmentManager().beginTransaction()
                .replace(R.id.container, Post.newInstance(ItemTable.getTitle(item), ItemTable.getDescription(item)))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new NewsLoader(getActivity(), mId, mChannel);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
        mSwipeLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
        mSwipeLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        getLoaderManager().restartLoader(R.id.loader_news, null, this);
    }

    private static class NewsAdapter extends CursorAdapter {

        public NewsAdapter(Context context) {
            super(context, null, false);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return View.inflate(context, android.R.layout.simple_list_item_1, null);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ((TextView) view.findViewById(android.R.id.text1)).setText(ItemTable.getTitle(cursor));
        }
    }
}
